package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// TODO
//
// 请定义 ParentEntity。其中 ParentEntity 不引用 ChildEntity。并且其对应的数据表定义应当满足如下
// 的条件：
//
// parent_entity
// +─────────+──────────────+──────────────────────────────+
// | Column  | Type         | Additional                   |
// +─────────+──────────────+──────────────────────────────+
// | id      | bigint       | primary key, auto_increment  |
// | name    | varchar(20)  | not null                     |
// +─────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ParentEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 20, nullable = false)
    private String name;

    public ParentEntity() {
    }

    public ParentEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
// --end-->

